package com.bankcrmproject.bankcrm;

import com.alibaba.fastjson.JSONObject;
import com.bankcrmproject.bankcrm.dataobject.CustomerDO;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.service.CustomerService;
import com.bankcrmproject.bankcrm.service.StaffService;
import com.bankcrmproject.bankcrm.service.model.CustomerProductSort;
import com.bankcrmproject.bankcrm.service.model.StaffModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankcrmApplicationTests {

    @Autowired
    private StaffService staffService;
    @Autowired
    private CustomerService customerService;

    @Test
    public void contextLoads() {

    }

    @Test
    public void textStaffService(){
        StaffModel staff = staffService.getStaffById(1);
        System.out.println(staff.getEmail());
    }

    @Test
    public void textSort(){
        List<CustomerProductSort> customerProductSort = customerService.getCustomerProductSort(1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("xx",customerProductSort);
        System.out.println(jsonObject);
    }
    @Test
    public void textGetAllStaffs() throws BusinessException {
        List<StaffModel> allStaffs = staffService.getAllStaffs(1, 2, 1, 10);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("xx",allStaffs);
        System.out.println(jsonObject);
    }

    @Test
    public void textStaffServiceUpdate(){
        StaffModel staffModel = new StaffModel();
        staffModel.setId(8);
        try {
            staffService.updateStaff(staffModel);
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }

}
