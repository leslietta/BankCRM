package com.bankcrmproject.bankcrm.error;

public enum EmBusinessError implements CommonError {
    //通用错误类型10001
    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    UNKNOW_ERROR(10002,"未知错误"),
    NO_PERMISSION(10003,"没有权限"),
    //2000开头的为用户信息相关错误定义
    USER_NOT_EXIST(20001,"用户不存在"),
    TOKEN_NOT_EXIST(20002,"Token不存在"),
    TOKEN_ERROR(20003,"Token错误"),
    USERNAME_OR_PASSWORD_ERROR(20004,"用户名或密码错误")
    ;


    private int errCode;
    private String errMsg;

    private EmBusinessError(int errCode,String errMsg){
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    @Override
    public int getErrCode() {
        return this.errCode;
    }

    @Override
    public String getErrMsg() {
        return this.errMsg;
    }

    @Override
    public CommonError setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }
}
