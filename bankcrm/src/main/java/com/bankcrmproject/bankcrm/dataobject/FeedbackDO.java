package com.bankcrmproject.bankcrm.dataobject;

import java.util.Date;

public class FeedbackDO {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.id
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.item
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private String item;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.code
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private String code;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.sort_id
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private Integer sortId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.state
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private Integer state;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.update_time
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private Date updateTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.create_time
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column feedback.detail
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    private String detail;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.id
     *
     * @return the value of feedback.id
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.id
     *
     * @param id the value for feedback.id
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.item
     *
     * @return the value of feedback.item
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public String getItem() {
        return item;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.item
     *
     * @param item the value for feedback.item
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setItem(String item) {
        this.item = item == null ? null : item.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.code
     *
     * @return the value of feedback.code
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public String getCode() {
        return code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.code
     *
     * @param code the value for feedback.code
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.sort_id
     *
     * @return the value of feedback.sort_id
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public Integer getSortId() {
        return sortId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.sort_id
     *
     * @param sortId the value for feedback.sort_id
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.state
     *
     * @return the value of feedback.state
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public Integer getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.state
     *
     * @param state the value for feedback.state
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.update_time
     *
     * @return the value of feedback.update_time
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.update_time
     *
     * @param updateTime the value for feedback.update_time
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.create_time
     *
     * @return the value of feedback.create_time
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.create_time
     *
     * @param createTime the value for feedback.create_time
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column feedback.detail
     *
     * @return the value of feedback.detail
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public String getDetail() {
        return detail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column feedback.detail
     *
     * @param detail the value for feedback.detail
     *
     * @mbg.generated Sat Apr 20 15:04:34 CST 2019
     */
    public void setDetail(String detail) {
        this.detail = detail == null ? null : detail.trim();
    }
}