package com.bankcrmproject.bankcrm.response;

/**
 * @Description:通用的返回对象
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/3/29 15:24
 */
public class CommonReturnType {
    //表明对应请求的返回处理结果“success”或者“fail”
    private String status;
    //若status为success，则data内为返回给前端的json数据
    //若status为fail，则data使用通用的错误码格式
    private Object data;

    //一个通用的创建方法
    public static CommonReturnType create(Object data){
        return CommonReturnType.create(data,"success");
    }

    public static CommonReturnType create(Object data,String status){
        CommonReturnType type = new CommonReturnType();
        type.setData(data);
        type.setStatus(status);
        return type;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
