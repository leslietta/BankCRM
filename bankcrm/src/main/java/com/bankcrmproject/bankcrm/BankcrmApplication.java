package com.bankcrmproject.bankcrm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.bankcrmproject.bankcrm"})
@MapperScan("com.bankcrmproject.bankcrm.dao")
public class BankcrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankcrmApplication.class, args);
    }

}
