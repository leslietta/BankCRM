package com.bankcrmproject.bankcrm.authorization.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description: 在controller的方法中使用此注解，检查是否登录，未登录返回401 un authorized
 * @see com.bankcrmproject.bankcrm.authorization.interceptor.AuthorizationInterceptor
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/10 20:05
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorization {
}
