package com.bankcrmproject.bankcrm.authorization.interceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.bankcrmproject.bankcrm.authorization.annotation.Authorization;
import com.bankcrmproject.bankcrm.config.Constants;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.error.EmBusinessError;
import com.bankcrmproject.bankcrm.service.StaffService;
import com.bankcrmproject.bankcrm.service.model.StaffModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @Description: 自定义拦截器，判断此请求是否有权限
 * @see com.bankcrmproject.bankcrm.authorization.annotation.Authorization
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/10 20:12
 */
@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private StaffService staffService;

    //在业务处理器处理请求之前被调用
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println(request.getRequestURI());
        //如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        String url = request.getRequestURI();
        //判断是否需要登录
        Authorization annotation = method.getAnnotation(Authorization.class);
        //有@Authorization注解，需要认证
        if ( annotation != null) {
            //从header中得到token
            String token = request.getHeader(Constants.AUTHORIZATION);
            if (token == null) {
                throw new BusinessException(EmBusinessError.TOKEN_NOT_EXIST);
            }
            //获取token中的userId
            String userId;
            try{
                userId = JWT.decode(token).getAudience().get(0);
            } catch (JWTDecodeException j) {
                throw new BusinessException(EmBusinessError.TOKEN_ERROR);
            }
            StaffModel staffModel = staffService.getStaffById(Integer.valueOf(userId));
            if (staffModel == null) {
                throw new BusinessException(EmBusinessError.USER_NOT_EXIST);
            }
            if (url.split("/")[2].equals("admin")){
                if (staffModel.getPrivilege() != 6){
                    throw new BusinessException(EmBusinessError.NO_PERMISSION);
                }
            }
            //验证token
            JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(staffModel.getEncrptPassword())).build();
            try{
                jwtVerifier.verify(token);
            } catch (JWTVerificationException e) {
                throw new BusinessException(EmBusinessError.TOKEN_ERROR);
            }
            request.setAttribute(Constants.CURRENT_USER_ID, staffModel.getId());
            System.out.println("通过拦截器");
            return true;
            // 不是 admin 不能访问 /admin 的接口
            /*if (url.split("/")[1].equals("admin")) {
                if (String.valueOf(model.getUserId()).length() != 3) {
                    System.out.println("ss");
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    return false;
                }
            }*/
        }else { //不需要登录
            System.out.println("跳过了");
            return true;
        }

    }
}
