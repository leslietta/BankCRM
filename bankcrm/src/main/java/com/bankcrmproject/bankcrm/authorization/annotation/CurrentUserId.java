package com.bankcrmproject.bankcrm.authorization.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description: 在Controller的方法参数中使用此注解，该方法在映射时会注入当前登录的userId
 * @see com.bankcrmproject.bankcrm.authorization.resolvers.CurrentUserIdMethodArgumentResolver
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/10 20:09
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface CurrentUserId {
}
