package com.bankcrmproject.bankcrm.controller;

import com.alibaba.fastjson.JSONObject;
import com.bankcrmproject.bankcrm.authorization.annotation.Authorization;
import com.bankcrmproject.bankcrm.authorization.annotation.CurrentUserId;
import com.bankcrmproject.bankcrm.dataobject.CustomerDO;
import com.bankcrmproject.bankcrm.dataobject.FeedbackDO;
import com.bankcrmproject.bankcrm.dataobject.ProductCustomerDO;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.error.EmBusinessError;
import com.bankcrmproject.bankcrm.response.CommonReturnType;
import com.bankcrmproject.bankcrm.service.CustomerService;
import com.bankcrmproject.bankcrm.service.model.CustomerProductSort;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import com.bankcrmproject.bankcrm.service.model.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description: 客户
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/23 14:08
 */

@RestController
@RequestMapping("/api/user/customer")
public class CustomerController extends BaseController {
    @Autowired
    private CustomerService customerService;

    @Authorization
    @PostMapping("/addCustomer")
    //仅经理可以调用
    public CommonReturnType addCustomer(CustomerDO customerDO,@CurrentUserId Integer staffId) throws BusinessException {
        if (customerDO == null) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        //调用service服务
        customerService.addCustomer(customerDO,staffId);
        return CommonReturnType.create(null);
    }
    @Authorization
    @PostMapping("/getCustomer")
    //通过customerId得到customer，皆可调用
    public CommonReturnType getCustomer(@RequestParam(name = "customerId",required = false) Integer customerId,
                                        @CurrentUserId Integer staffId) throws BusinessException {
        if (customerId == null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        System.out.println(customerId+staffId);
        //调用service服务
        CustomerDO customerDO = customerService.getCustomerByCustomerId(staffId,customerId);
        return CommonReturnType.create(customerDO);
    }
    @Authorization
    @PostMapping("/updateCustomer")
    //通过customerId得到customer，皆可调用
    public CommonReturnType updateCustomer(CustomerDO customerDO,
                                        @CurrentUserId Integer staffId) throws BusinessException {
        if (customerDO == null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        //调用service服务
        customerService.updateCustomer(customerDO,staffId);
        return CommonReturnType.create(null);
    }
    @Authorization
    @PostMapping("/buyProductForCustomer")
    //皆可调用
    public CommonReturnType buyProductForCustomer(@RequestParam(value = "customerId",required = false) Integer customerId,
                                        @RequestParam(value = "productId",required =false) Integer productId,
                                        @CurrentUserId Integer staffId) throws BusinessException {
        if (customerId == null || productId == null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        //调用service服务
        customerService.buyProductForCustomer(customerId,productId,staffId);
        return CommonReturnType.create(null);
    }

    @Authorization
    @PostMapping("/getCustomersByStaffId")
    //通过staffId得到旗下所有的customer，皆可
    public CommonReturnType getCustomersByStaffId(@RequestParam(value = "staffId",required = false) Integer staffId,
                                                  @RequestParam(name = "page") Integer page,
                                                  @RequestParam(name = "rows") Integer rows,
                                                  @CurrentUserId Integer currentStaffId) throws BusinessException {
        if (staffId == null){
            //未staffId，默认设为当前登录staff
            staffId = currentStaffId;
            //throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<CustomerDO> customerDOList = customerService.getCustomersByStaffId(staffId,page,rows);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("customerDOList",customerDOList);
        jsonObject.put("total",customerDOList.size());
        return CommonReturnType.create(jsonObject);
    }

    @Authorization
    @PostMapping("/getProductModelByCustomerId")
    //通过customerId和productId得到订单信息
    public CommonReturnType getProductModelByCustomerId(ProductCustomerDO productCustomerDO) {
        ProductModel productModel = customerService.getProductModelByCustomerId(productCustomerDO);
        return CommonReturnType.create(productModel);
    }
    @Authorization
    @PostMapping("/getCustomerProductSort")
    //通过customerId得到其办理的产品类别
    public CommonReturnType getCustomerProductSort(@RequestParam(name = "customerId") Integer customerId) {
        List<CustomerProductSort> customerProductSortList = customerService.getCustomerProductSort(customerId);
        return CommonReturnType.create(customerProductSortList);
    }

    @Authorization
    @PostMapping("/getProductsByCustomerId")
    //通过customerId得到购买的所有product
    public CommonReturnType getProductsByCustomerId(@RequestParam(value = "customerId",required = false) Integer customerId,
                                                    @RequestParam(name = "page") Integer page,
                                                    @RequestParam(name = "rows") Integer rows) throws BusinessException {
        if (customerId == null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<ProductModel> productModelList = customerService.getProductsByCustomerId(customerId,page,rows);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("productModelList",productModelList);
        jsonObject.put("total",productModelList.size());
        return CommonReturnType.create(jsonObject);
    }
    @Authorization
    @PostMapping("/getFeedbacksByCustomerId")
    //通过customerId得到客户所有的反馈
    public CommonReturnType getFeedbacksByCustomerId(@RequestParam(name = "customerId") Integer customerId){
        List<FeedbackModel> feedbackModelList = customerService.getFeedbacksByCustomerId(customerId);
        return CommonReturnType.create(feedbackModelList);
    }

}
