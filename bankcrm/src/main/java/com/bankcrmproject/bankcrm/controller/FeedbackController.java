package com.bankcrmproject.bankcrm.controller;

import com.bankcrmproject.bankcrm.authorization.annotation.Authorization;
import com.bankcrmproject.bankcrm.dataobject.CustomerDO;
import com.bankcrmproject.bankcrm.dataobject.FeedbackDO;
import com.bankcrmproject.bankcrm.dataobject.ProductDO;
import com.bankcrmproject.bankcrm.response.CommonReturnType;
import com.bankcrmproject.bankcrm.service.FeedbackService;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 反馈相关
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/5/11 16:25
 */

@RestController
@RequestMapping("/api/user/feedback")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    @Authorization
    @PostMapping("/getProductFeedbacks")
    //得到关于产品的所有反馈
    public CommonReturnType getProductFeedbacks(@RequestParam(name = "page") Integer page,
                                                @RequestParam(name = "rows") Integer rows){
        List<FeedbackModel> feedbackModelList = feedbackService.getProductFeedbacks(page,rows);
        return CommonReturnType.create(feedbackModelList);
    }
    @Authorization
    @PostMapping("/getCustomerFeedbacks")
    //得到关于客户的所有反馈
    public CommonReturnType getCustomerFeedbacks(@RequestParam(name = "page") Integer page,
                                                @RequestParam(name = "rows") Integer rows){
        List<FeedbackModel> feedbackModelList = feedbackService.getCustomerFeedbacks(page,rows);
        return CommonReturnType.create(feedbackModelList);
    }
    @Authorization
    @PostMapping("/addCustomerFeedback")
    //添加客户的反馈
    public CommonReturnType addCustomerFeedback(@RequestParam(name = "customerId") Integer customerId,
                                                FeedbackDO feedbackDO){
        feedbackService.addCustomerFeedback(customerId,feedbackDO);
        return CommonReturnType.create(null);
    }
    @Authorization
    @PostMapping("/addProductFeedback")
    //添加产品的反馈
    public CommonReturnType addProductFeedback(@RequestParam(name = "productId") Integer productId,
                                               FeedbackDO feedbackDO){
        feedbackService.addProductFeedback(productId,feedbackDO);
        return CommonReturnType.create(null);
    }
    @Authorization
    @PostMapping("/getCustomerByCustomerCode")
    //根据customerCode得到customer
    public CommonReturnType getCustomerByCustomerCode(@RequestParam(name = "customerCode") Integer customerCode){
        CustomerDO customerDO = feedbackService.getCustomerByCustomerCode(customerCode);
        return CommonReturnType.create(customerDO);
    }
    @Authorization
    @PostMapping("/getProductByProductCode")
    //根据productCode得到product
    public CommonReturnType getProductByProductCode(@RequestParam(name = "productCode") Integer productCode){
        ProductDO productDO = feedbackService.getProductByProductCode(productCode);
        return CommonReturnType.create(productDO);
    }
    @Authorization
    @PostMapping("/getFeedbackByFeedbackCode")
    //根据feedbackCode得到feedback
    public CommonReturnType getFeedbackByFeedbackCode(@RequestParam(name = "feedbackCode") Integer feedbackCode){
        FeedbackDO feedbackDO = feedbackService.getFeedbackByFeedbackCode(feedbackCode);
        return CommonReturnType.create(feedbackDO);
    }

}
