package com.bankcrmproject.bankcrm.controller;

import com.alibaba.fastjson.JSONObject;
import com.bankcrmproject.bankcrm.authorization.annotation.Authorization;
import com.bankcrmproject.bankcrm.dataobject.ProductDO;
import com.bankcrmproject.bankcrm.dataobject.SettingDO;
import com.bankcrmproject.bankcrm.response.CommonReturnType;
import com.bankcrmproject.bankcrm.service.ProductService;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import com.bankcrmproject.bankcrm.service.model.ProductModel;
import com.bankcrmproject.bankcrm.service.model.ProductSort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 产品相关
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/5/8 17:15
 */

@RestController
@RequestMapping("/api/user/product")
public class ProductController extends BaseController {
    @Autowired
    private ProductService productService;

    @Authorization
    @PostMapping("/getProductSorts")
    //得到已有产品的分类
    public CommonReturnType getProductSorts(){
        List<ProductSort> productSortList = productService.getProductSorts();
        return CommonReturnType.create(productSortList);
    }
    @Authorization
    @PostMapping("/getAllProductSorts")
    //得到所有产品的分类
    public CommonReturnType getAllProductSorts(){
        List<SettingDO> settingDOList = productService.getAllProductSorts();
        return CommonReturnType.create(settingDOList);
    }

    @Authorization
    @PostMapping("/getProductsBySortId")
    //根据分类得到产品
    public CommonReturnType getProductsBySortId(@RequestParam(name = "sortId") Integer sortId){
        List<ProductDO> productDOList = productService.getProductsBySortId(sortId);
        return CommonReturnType.create(productDOList);
    }
    @Authorization
    @PostMapping("/getProductByProductId")
    //根据Id得到产品
    public CommonReturnType getProductByProductId(@RequestParam(name = "productId") Integer productId){
        ProductModel productModel = productService.getProductByProductId(productId);
        return CommonReturnType.create(productModel);
    }

    @Authorization
    @PostMapping("/getAllProducts")
    //得到所有产品
    public CommonReturnType getAllProducts(@RequestParam(name = "page") Integer page,
                                           @RequestParam(name = "rows") Integer rows){
        List<ProductModel> productModelList = productService.getAllProducts(page,rows);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("customerDOList",productModelList);
        jsonObject.put("total",productModelList.size());
        return CommonReturnType.create(jsonObject);
    }

    @Authorization
    @PostMapping("/addProduct")
    //添加产品
    public CommonReturnType addProduct(ProductDO productDO){
        productService.addProduct(productDO);
        return CommonReturnType.create(null);
    }
    @Authorization
    @PostMapping("/updateProduct")
    //更新产品
    public CommonReturnType updateProduct(ProductDO productDO){
        productService.updateProduct(productDO);
        return CommonReturnType.create(null);
    }
    @Authorization
    @PostMapping("/updateSetting")
    //更新分类
    public CommonReturnType updateSetting(SettingDO settingDO){
        productService.updateSetting(settingDO);
        return CommonReturnType.create(null);
    }
    @Authorization
    @PostMapping("/getFeedbacksByProductId")
    //得到产品的所有反馈
    public CommonReturnType getFeedbacksByProductId(@RequestParam(name = "productId") Integer productId){
        List<FeedbackModel> feedbackModelList = productService.getFeedbacksByProductId(productId);
        return CommonReturnType.create(feedbackModelList);
    }


}
