package com.bankcrmproject.bankcrm.controller;
import com.alibaba.fastjson.JSONObject;
import com.bankcrmproject.bankcrm.authorization.annotation.Authorization;
import com.bankcrmproject.bankcrm.authorization.annotation.CurrentUserId;
import com.bankcrmproject.bankcrm.controller.viewobject.StaffVO;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.error.EmBusinessError;
import com.bankcrmproject.bankcrm.response.CommonReturnType;
import com.bankcrmproject.bankcrm.service.StaffService;
import com.bankcrmproject.bankcrm.service.model.StaffModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: 用户
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/3/28 22:03
 */
@RestController
@RequestMapping("/api/admin/staff")
public class StaffController extends BaseController{
    @Autowired
    private StaffService staffService;

    @Authorization
    @PostMapping(value = "/getStaff")
    public CommonReturnType getStaff(@RequestParam( name = "id") Integer id) throws BusinessException {
        //调用service服务
        StaffModel staffModel = staffService.getStaffById(id);

        if(staffModel == null){
            throw new BusinessException(EmBusinessError.USER_NOT_EXIST);
            //staffModel.setEncrptPassword("123");
        }
        //将核心模型转化为供ui使用的vo
        StaffVO staffVO = convertFromModel(staffModel);
        //返回通用的对象
        return CommonReturnType.create(staffVO);
    }

    @Authorization
    @PostMapping(value = "/getAllStaffs")
    public CommonReturnType getAllStaffs(@RequestParam(name = "page") Integer page,
                                      @RequestParam(name = "rows") Integer rows,
                                      @RequestParam(name = "privilege") Integer privilege,
                                      @CurrentUserId Integer currentStaffId) throws BusinessException {
        List<StaffModel> staffModelList = staffService.getAllStaffs(currentStaffId,privilege,page, rows);
        List<StaffVO> staffVOList = staffModelList.stream().map(staffModel -> {
            StaffVO staffVO = this.convertFromModel(staffModel);
            return staffVO;
        }).collect(Collectors.toList());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("staffVOList",staffVOList);
        jsonObject.put("total",staffVOList.size());
        return CommonReturnType.create(jsonObject);
    }

    @Authorization
    @PostMapping(value = "/addStaff")
    public CommonReturnType addStaff(StaffModel staffModel) throws BusinessException {
        if (staffModel == null ){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        staffService.addStaff(staffModel);
        return CommonReturnType.create(null);
    }

    @Authorization
    @PostMapping(value = "/updateStaff")
    public CommonReturnType updateStaff(StaffModel staffModel) throws BusinessException {
        if (staffModel == null ){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        staffService.updateStaff(staffModel);
        return CommonReturnType.create(null);
    }

    @Authorization
    @PostMapping(value = "/deleteStaff")
    public CommonReturnType deleteStaff(@RequestParam(name = "id",required=false) Integer id) throws BusinessException {
        if (id == null ){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        staffService.deleteStaff(id);
        return CommonReturnType.create(null);
    }



    private StaffVO convertFromModel(StaffModel staffModel){
        if(staffModel == null){
            return null;
        }
        StaffVO staffVO = new StaffVO();
        BeanUtils.copyProperties(staffModel, staffVO);
        return staffVO;
    }


}
