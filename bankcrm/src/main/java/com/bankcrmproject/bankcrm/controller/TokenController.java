package com.bankcrmproject.bankcrm.controller;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.bankcrmproject.bankcrm.authorization.annotation.Authorization;
import com.bankcrmproject.bankcrm.authorization.annotation.CurrentUserId;
import com.bankcrmproject.bankcrm.controller.viewobject.StaffVO;
import com.bankcrmproject.bankcrm.dataobject.CustomerDO;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.error.EmBusinessError;
import com.bankcrmproject.bankcrm.response.CommonReturnType;
import com.bankcrmproject.bankcrm.service.CustomerService;
import com.bankcrmproject.bankcrm.service.TokenService;
import com.bankcrmproject.bankcrm.service.StaffService;
import com.bankcrmproject.bankcrm.service.model.StaffModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description: 处理用户登录和token刷新
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/13 22:23
 */
@Controller("token")
@RequestMapping("/api/auth")
public class TokenController extends BaseController {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private StaffService staffService;

    @RequestMapping("/login")
    @ResponseBody
    public CommonReturnType login(@RequestParam(name = "email") String email,
                                  @RequestParam(name = "password") String password) throws BusinessException {
        System.out.println("进来了"+email+password);
        JSONObject jsonObject=new JSONObject();
        StaffModel staffModel = staffService.getStaffByEmail(email);
        if (staffModel == null) {
            throw new BusinessException(EmBusinessError.USER_NOT_EXIST);
        }else {
            if(!staffModel.getEncrptPassword().equals(password)) {
                throw new BusinessException(EmBusinessError.USERNAME_OR_PASSWORD_ERROR);
            } else {
                String token = tokenService.getToken(staffModel);
                jsonObject.put("token",token);
                jsonObject.put("user", staffModel);
                return CommonReturnType.create(jsonObject);
            }
        }
    }

    @RequestMapping("/refresh")
    @ResponseBody
    //刷新token
    public CommonReturnType refresh(@RequestParam(name = "token") String token) throws BusinessException {
        //获取token中的userId
        String userId;
        try{
            userId = JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException j) {
            throw new BusinessException(EmBusinessError.TOKEN_ERROR);
        }
        StaffModel staffModel = staffService.getStaffById(Integer.valueOf(userId));
        if (staffModel == null) {
            throw new BusinessException(EmBusinessError.USER_NOT_EXIST);
        }
        //验证token
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(staffModel.getEncrptPassword())).build();
        try{
            jwtVerifier.verify(token);
        } catch (JWTVerificationException e) {
            throw new BusinessException(EmBusinessError.TOKEN_ERROR);
        }
        String newToken = tokenService.getToken(staffModel);
        return CommonReturnType.create(newToken);
    }


    @Authorization
    @RequestMapping("/info")
    @ResponseBody
    public CommonReturnType getMessage(@CurrentUserId Integer userId) throws BusinessException {
        //调用service服务
        StaffModel staffModel = staffService.getStaffById(userId);

        if(staffModel == null){
            throw new BusinessException(EmBusinessError.USER_NOT_EXIST);
            //staffModel.setEncrptPassword("123");
        }
        //将核心模型转化为供ui使用的vo
        StaffVO staffVO = convertFromModel(staffModel);
        //返回通用的对象
        return CommonReturnType.create(staffVO);
    }

    private StaffVO convertFromModel(StaffModel staffModel){
        if(staffModel == null){
            return null;
        }
        StaffVO staffVO = new StaffVO();
        BeanUtils.copyProperties(staffModel, staffVO);
        return staffVO;
    }

}
