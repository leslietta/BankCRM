package com.bankcrmproject.bankcrm.dao;

import com.bankcrmproject.bankcrm.dataobject.StaffPasswordDO;

public interface StaffPasswordDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff_password
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff_password
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int insert(StaffPasswordDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff_password
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int insertSelective(StaffPasswordDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff_password
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    StaffPasswordDO selectByPrimaryKey(Integer id);

    StaffPasswordDO selectByStaffId(Integer staffId);

    int updateByStaffIdSelective(StaffPasswordDO record);

    int deleteByStaffId(Integer staffId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff_password
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int updateByPrimaryKeySelective(StaffPasswordDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff_password
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int updateByPrimaryKey(StaffPasswordDO record);
}