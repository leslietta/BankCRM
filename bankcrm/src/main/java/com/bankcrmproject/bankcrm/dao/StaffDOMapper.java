package com.bankcrmproject.bankcrm.dao;

import com.bankcrmproject.bankcrm.dataobject.StaffDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StaffDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int insert(StaffDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int insertSelective(StaffDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    StaffDO selectByPrimaryKey(Integer id);

    StaffDO selectByEmail(String email);

    List<StaffDO> selectAllStaffs(@Param("privilege")Integer privilege);


    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int updateByPrimaryKeySelective(StaffDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table staff
     *
     * @mbg.generated Mon Apr 22 20:17:21 CST 2019
     */
    int updateByPrimaryKey(StaffDO record);
}