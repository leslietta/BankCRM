package com.bankcrmproject.bankcrm.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.bankcrmproject.bankcrm.service.TokenService;
import com.bankcrmproject.bankcrm.service.model.StaffModel;
import org.springframework.stereotype.Service;

/**
 * @Description: 生成token
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/13 17:55
 */
@Service
public class TokenServiceImpl implements TokenService {
    @Override
    public String getToken(StaffModel staffModel) {
        String token="";
        token= JWT.create().withAudience(String.valueOf(staffModel.getId()))// 将 user id 保存到 token 里面
                .sign(Algorithm.HMAC256(staffModel.getEncrptPassword()));// 以 password 作为 token 的密钥
        return token;
    }
}
