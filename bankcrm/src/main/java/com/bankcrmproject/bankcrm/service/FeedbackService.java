package com.bankcrmproject.bankcrm.service;

import com.bankcrmproject.bankcrm.dataobject.CustomerDO;
import com.bankcrmproject.bankcrm.dataobject.FeedbackDO;
import com.bankcrmproject.bankcrm.dataobject.ProductDO;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;

import java.util.List;

public interface FeedbackService {

    List<FeedbackModel> getProductFeedbacks(Integer page, Integer rows);

    List<FeedbackModel> getCustomerFeedbacks(Integer page, Integer rows);

    void addCustomerFeedback(Integer customerId, FeedbackDO feedbackDO);

    void addProductFeedback(Integer productId, FeedbackDO feedbackDO);

    CustomerDO getCustomerByCustomerCode(Integer customerCode);

    ProductDO getProductByProductCode(Integer productCode);

    FeedbackDO getFeedbackByFeedbackCode(Integer feedbackCode);
}
