package com.bankcrmproject.bankcrm.service.impl;
import com.bankcrmproject.bankcrm.dao.StaffDOMapper;
import com.bankcrmproject.bankcrm.dao.StaffPasswordDOMapper;
import com.bankcrmproject.bankcrm.dataobject.StaffDO;
import com.bankcrmproject.bankcrm.dataobject.StaffPasswordDO;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.error.EmBusinessError;
import com.bankcrmproject.bankcrm.service.StaffService;
import com.bankcrmproject.bankcrm.service.model.StaffModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/3/28 22:17
 */
@Service
public class StaffServiceImpl implements StaffService {
    @Autowired
    private StaffDOMapper staffDOMapper;
    @Autowired
    private StaffPasswordDOMapper staffPasswordDOMapper;


    @Override
    public StaffModel getStaffById(Integer id) {
        StaffDO staffDO = staffDOMapper.selectByPrimaryKey(id);
        if(staffDO == null) {
            return null;
        }
        StaffPasswordDO staffPasswordDO = staffPasswordDOMapper.selectByStaffId(staffDO.getId());
        return convertFromDataObject(staffDO,staffPasswordDO);

    }

    @Override
    public StaffModel getStaffByEmail(String email) {
        StaffDO staffDO = staffDOMapper.selectByEmail(email);
        if (staffDO == null) {
            return null;
        }
        StaffPasswordDO staffPasswordDO = staffPasswordDOMapper.selectByStaffId(staffDO.getId());
        return convertFromDataObject(staffDO,staffPasswordDO);
    }

    @Override
    public List<StaffModel> getAllStaffs(Integer staffId,Integer privilege,Integer page, Integer rows) throws BusinessException {
        StaffDO staffDO1 = staffDOMapper.selectByPrimaryKey(staffId);
        if (staffDO1.getPrivilege() != 6){
            throw new BusinessException(EmBusinessError.NO_PERMISSION);
        }
        if( page !=null && rows != null ){
            PageHelper.startPage(page,rows);
        }
        List<StaffDO> staffDOList = staffDOMapper.selectAllStaffs(privilege);
        List<StaffModel> staffModelList = staffDOList.stream().map(staffDO -> {
            StaffPasswordDO staffPasswordDO = staffPasswordDOMapper.selectByStaffId(staffDO.getId());
            StaffModel staffModel = this.convertFromDataObject(staffDO,staffPasswordDO);
            return staffModel;
        }).collect(Collectors.toList());
        return staffModelList;

    }

    @Override
    @Transactional
    public void addStaff(StaffModel staffModel) throws BusinessException {
        //model--->dataobject
        StaffDO staffDO = this.convertStaffFromModel(staffModel);
        try{
            //使用insertSelective会判断字段是否为空，为空不会插入，会使用数据库默认设置
            staffDOMapper.insertSelective(staffDO);
        }catch (DuplicateKeyException e){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"邮箱已被使用");
        }
        //mybatis会将新id传给staffDO
        staffModel.setId(staffDO.getId());
        //model--->dataobject
        StaffPasswordDO staffPasswordDO = this.convertPasswordFromModel(staffModel);
        staffPasswordDOMapper.insertSelective(staffPasswordDO);
    }

    @Override
    @Transactional
    public void updateStaff(StaffModel staffModel) throws BusinessException {
        //model--->dataobject
        StaffDO staffDO = this.convertStaffFromModel(staffModel);
        if (staffDO.getAge() !=null || staffDO.getCode() != null || staffDO.getEmail() != null
                || staffDO.getGender() != null || staffDO.getName() != null){
            try{
                //使用updateByPrimaryKeySelective会判断字段是否为空，为空不会插入，会使用数据库默认设置
                staffDOMapper.updateByPrimaryKeySelective(staffDO);
            }catch (DuplicateKeyException e){
                throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"邮箱已被使用");
            }
        }
        //model--->dataobject
        if (staffModel.getEncrptPassword() != null){
            StaffPasswordDO staffPasswordDO = this.convertPasswordFromModel(staffModel);
            staffPasswordDOMapper.updateByStaffIdSelective(staffPasswordDO);
        }
        return;
    }

    @Override
    @Transactional
    public void deleteStaff(Integer id) {
        staffDOMapper.deleteByPrimaryKey(id);
        staffPasswordDOMapper.deleteByStaffId(id);
        return;
    }

    private StaffDO convertStaffFromModel(StaffModel staffModel){
        if (staffModel == null) {
            return null;
        }
        StaffDO staffDO = new StaffDO();
        BeanUtils.copyProperties(staffModel,staffDO);
        return staffDO;
    }

    private StaffPasswordDO convertPasswordFromModel(StaffModel staffModel){
        if (staffModel == null) {
            return null;
        }
        StaffPasswordDO staffPasswordDO = new StaffPasswordDO();
        BeanUtils.copyProperties(staffModel,staffPasswordDO);
        staffPasswordDO.setStaffId(staffModel.getId());
        return staffPasswordDO;
    }


    private StaffModel convertFromDataObject(StaffDO staffDO, StaffPasswordDO staffPasswordDO){
        if (staffDO == null){
            return null;
        }
        StaffModel staffModel = new StaffModel();
        BeanUtils.copyProperties(staffDO, staffModel);
        if(staffPasswordDO != null){
            staffModel.setEncrptPassword(staffPasswordDO.getEncrptPassword());
        }
        return staffModel;
    }
}
