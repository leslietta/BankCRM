package com.bankcrmproject.bankcrm.service;

import com.bankcrmproject.bankcrm.dataobject.ProductDO;
import com.bankcrmproject.bankcrm.dataobject.SettingDO;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import com.bankcrmproject.bankcrm.service.model.ProductModel;
import com.bankcrmproject.bankcrm.service.model.ProductSort;

import java.util.List;

public interface ProductService {

    List<ProductSort> getProductSorts();

    List<ProductDO> getProductsBySortId(Integer sortId);

    List<ProductModel> getAllProducts(Integer page, Integer rows);

    ProductModel getProductByProductId(Integer productId);

    List<SettingDO> getAllProductSorts();

    void addProduct(ProductDO productDO);

    void updateProduct(ProductDO productDO);

    void updateSetting(SettingDO settingDO);

    List<FeedbackModel> getFeedbacksByProductId(Integer productId);
}
