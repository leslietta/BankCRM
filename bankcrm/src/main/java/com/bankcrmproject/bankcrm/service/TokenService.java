package com.bankcrmproject.bankcrm.service;

import com.bankcrmproject.bankcrm.service.model.StaffModel;

/**
 * @Description: 生成token
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/13 17:51
 */
public interface TokenService {
    public String getToken(StaffModel user);

}
