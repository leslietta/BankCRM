package com.bankcrmproject.bankcrm.service.impl;

import com.bankcrmproject.bankcrm.dao.*;
import com.bankcrmproject.bankcrm.dataobject.*;
import com.bankcrmproject.bankcrm.service.ProductService;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import com.bankcrmproject.bankcrm.service.model.ProductModel;
import com.bankcrmproject.bankcrm.service.model.ProductSort;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/5/8 17:24
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductCustomerDOMapper productCustomerDOMapper;
    @Autowired
    private ProductDOMapper productDOMapper;
    @Autowired
    private SettingDOMapper settingDOMapper;
    @Autowired
    private ProductFeedbackDOMapper productFeedbackDOMapper;
    @Autowired
    private FeedbackDOMapper feedbackDOMapper;
    //得到产品的分类情况
    @Override
    public List<ProductSort> getProductSorts() {
        List<ProductSort> productSortList = productDOMapper.selectProductSorts();
        return productSortList;
    }

    @Override
    public List<ProductDO> getProductsBySortId(Integer sortId) {
        List<ProductDO> productDOList = productDOMapper.selectBySortId(sortId);
        return productDOList;
    }

    @Override
    public List<ProductModel> getAllProducts(Integer page, Integer rows) {
        PageHelper.startPage(page,rows);
        List<ProductDO> productDOList = productDOMapper.selectAllProducts();
        List<ProductModel> productModelList = productDOList.stream().map(productDO -> {
            SettingDO settingDO = settingDOMapper.selectByPrimaryKey(productDO.getSortId());
            ProductModel productModel = this.convertFromDataObject(productDO, settingDO);
            return productModel;
        }).collect(Collectors.toList());
        return productModelList;
    }

    @Override
    public ProductModel getProductByProductId(Integer productId) {
        ProductDO productDO = productDOMapper.selectByPrimaryKey(productId);
        if (productDO != null){
            ProductModel productModel = new ProductModel();
            BeanUtils.copyProperties(productDO,productModel);
            SettingDO settingDO = settingDOMapper.selectByPrimaryKey(productDO.getSortId());
            productModel.setSort(settingDO.getValue());
            return productModel;
        }
        return null;
    }

    @Override
    public List<SettingDO> getAllProductSorts() {
        List<SettingDO> settingDOList = settingDOMapper.selectByType(1);
        return settingDOList;
    }

    @Override
    public void addProduct(ProductDO productDO) {
         productDOMapper.insertSelective(productDO);
    }

    @Override
    public void updateProduct(ProductDO productDO) {
        productDOMapper.updateByPrimaryKeySelective(productDO);
    }

    @Override
    public void updateSetting(SettingDO settingDO) {
        settingDOMapper.updateByPrimaryKeySelective(settingDO);
    }

    @Override
    public List<FeedbackModel> getFeedbacksByProductId(Integer productId) {
        List<ProductFeedbackDO> productFeedbackDOList = productFeedbackDOMapper.selectProductFeedbackDOByProductId(productId);
        List<FeedbackModel> feedbackModelList = productFeedbackDOList.stream().map(productFeedbackDO -> {
            FeedbackDO feedbackDO = feedbackDOMapper.selectByPrimaryKey(productFeedbackDO.getFeedbackId());
            SettingDO settingDO = settingDOMapper.selectByPrimaryKey(feedbackDO.getSortId());
            FeedbackModel feedbackModel = this.convertFromFeedbackDO(feedbackDO, settingDO);
            return feedbackModel;
        }).collect(Collectors.toList());
        return feedbackModelList;
    }

    private ProductModel convertFromDataObject(ProductDO productDO, SettingDO settingDO){
        if (productDO!=null&&settingDO!=null){
            ProductModel productModel = new ProductModel();
            BeanUtils.copyProperties(productDO,productModel);
            productModel.setSort(settingDO.getValue());
            return productModel;
        }
        return null;
    }

    private FeedbackModel convertFromFeedbackDO(FeedbackDO feedbackDO, SettingDO settingDO){
        FeedbackModel feedbackModel = new FeedbackModel();
        BeanUtils.copyProperties(feedbackDO,feedbackModel);
        feedbackModel.setSortName(settingDO.getValue());
        return feedbackModel;
    }

}
