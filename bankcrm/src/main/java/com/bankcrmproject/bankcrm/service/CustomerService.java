package com.bankcrmproject.bankcrm.service;

import com.alibaba.fastjson.JSONObject;
import com.bankcrmproject.bankcrm.dataobject.CustomerDO;
import com.bankcrmproject.bankcrm.dataobject.FeedbackDO;
import com.bankcrmproject.bankcrm.dataobject.ProductCustomerDO;
import com.bankcrmproject.bankcrm.dataobject.ProductDO;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.service.model.CustomerProductSort;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import com.bankcrmproject.bankcrm.service.model.ProductModel;

import java.util.List;

/**
 * @Description:
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/22 11:00
 */
public interface CustomerService {

    void addCustomer(CustomerDO customerDO,Integer staffId) throws BusinessException;

    void updateCustomer(CustomerDO customerDO,Integer staffId) throws BusinessException;

    void buyProductForCustomer(Integer customerId,Integer productId,Integer staffId) throws BusinessException;

    CustomerDO getCustomerByCustomerId(Integer staffId,Integer customerId) throws BusinessException;
    

    List<CustomerDO> getCustomersByStaffId(Integer staffId,Integer page,Integer rows);

    List<ProductModel> getProductsByCustomerId(Integer customerId,Integer page,Integer rows);

    ProductModel getProductModelByCustomerId(ProductCustomerDO productCustomerDO);

    List<CustomerProductSort> getCustomerProductSort(Integer customerId);

    List<FeedbackModel> getFeedbacksByCustomerId(Integer customerId);
}
