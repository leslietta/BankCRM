package com.bankcrmproject.bankcrm.service.impl;

import com.bankcrmproject.bankcrm.dao.*;
import com.bankcrmproject.bankcrm.dataobject.*;
import com.bankcrmproject.bankcrm.service.FeedbackService;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/5/11 16:36
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private FeedbackDOMapper feedbackDOMapper;
    @Autowired
    private ProductFeedbackDOMapper productFeedbackDOMapper;
    @Autowired
    private SettingDOMapper settingDOMapper;
    @Autowired
    private FeedbackCustomerDOMapper feedbackCustomerDOMapper;
    @Autowired
    private CustomerDOMapper customerDOMapper;
    @Autowired
    private ProductDOMapper productDOMapper;

    @Override
    public List<FeedbackModel> getProductFeedbacks(Integer page, Integer rows) {
        PageHelper.startPage(page,rows);
        List<ProductFeedbackDO> productFeedbackDOList = productFeedbackDOMapper.selectAll();
        List<FeedbackModel> feedbackModelList = productFeedbackDOList.stream().map(productFeedbackDO -> {
            FeedbackDO feedbackDO = feedbackDOMapper.selectByPrimaryKey(productFeedbackDO.getFeedbackId());
            SettingDO settingDO = settingDOMapper.selectByPrimaryKey(feedbackDO.getSortId());
            FeedbackModel feedbackModel = this.convertFromFeedbackDO(feedbackDO, settingDO);
            return feedbackModel;
        }).collect(Collectors.toList());
        return feedbackModelList;
    }

    @Override
    public List<FeedbackModel> getCustomerFeedbacks(Integer page, Integer rows) {
        PageHelper.startPage(page,rows);
        List<FeedbackCustomerDO> feedbackCustomerDOList = feedbackCustomerDOMapper.selectAll();
        List<FeedbackModel> feedbackModelList = feedbackCustomerDOList.stream().map(feedbackCustomerDO -> {
            FeedbackDO feedbackDO = feedbackDOMapper.selectByPrimaryKey(feedbackCustomerDO.getFeedbackId());
            SettingDO settingDO = settingDOMapper.selectByPrimaryKey(feedbackDO.getSortId());
            FeedbackModel feedbackModel = this.convertFromFeedbackDO(feedbackDO, settingDO);
            return feedbackModel;
        }).collect(Collectors.toList());
        return feedbackModelList;
    }

    @Override
    @Transactional
    public void addCustomerFeedback(Integer customerId, FeedbackDO feedbackDO) {
        feedbackDOMapper.insertSelective(feedbackDO);
        FeedbackCustomerDO feedbackCustomerDO = new FeedbackCustomerDO();
        feedbackCustomerDO.setCustomerId(customerId);
        feedbackCustomerDO.setFeedbackId(feedbackDO.getId());
        feedbackCustomerDOMapper.insertSelective(feedbackCustomerDO);
    }

    @Override
    @Transactional
    public void addProductFeedback(Integer productId, FeedbackDO feedbackDO) {
        feedbackDOMapper.insertSelective(feedbackDO);
        ProductFeedbackDO productFeedbackDO = new ProductFeedbackDO();
        productFeedbackDO.setProductId(productId);
        productFeedbackDO.setFeedbackId(feedbackDO.getId());
        productFeedbackDOMapper.insertSelective(productFeedbackDO);
    }

    @Override
    public CustomerDO getCustomerByCustomerCode(Integer customerCode) {
        return customerDOMapper.selectByCode(customerCode);
    }

    @Override
    public ProductDO getProductByProductCode(Integer productCode) {
        return productDOMapper.selectByCode(productCode);
    }

    @Override
    public FeedbackDO getFeedbackByFeedbackCode(Integer feedbackCode) {
        return feedbackDOMapper.selectByCode(feedbackCode);
    }


    private FeedbackModel convertFromFeedbackDO(FeedbackDO feedbackDO, SettingDO settingDO){
        FeedbackModel feedbackModel = new FeedbackModel();
        BeanUtils.copyProperties(feedbackDO,feedbackModel);
        feedbackModel.setSortName(settingDO.getValue());
        return feedbackModel;
    }
}
