package com.bankcrmproject.bankcrm.service.model;

/**
 * @Description: 客户办理产品的类别
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/5/8 11:33
 */
public class CustomerProductSort {
    private Integer customerId; //客户id
    private Integer sortId; //sort_id
    private Integer total;  //该类型总数
    private String sortName; //类型名称

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }
}
