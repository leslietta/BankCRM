package com.bankcrmproject.bankcrm.service.model;

/**
 * @Description: 产品的分类
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/5/8 17:20
 */
public class ProductSort {

    private String sortName;
    private Integer sortId;

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }
}
