package com.bankcrmproject.bankcrm.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bankcrmproject.bankcrm.dao.*;
import com.bankcrmproject.bankcrm.dataobject.*;
import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.error.EmBusinessError;
import com.bankcrmproject.bankcrm.service.CustomerService;
import com.bankcrmproject.bankcrm.service.model.CustomerProductSort;
import com.bankcrmproject.bankcrm.service.model.FeedbackModel;
import com.bankcrmproject.bankcrm.service.model.ProductModel;
import com.bankcrmproject.bankcrm.utils.MemberUtil;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/22 11:02
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDOMapper customerDOMapper;
    @Autowired
    private StaffCustomerDOMapper staffCustomerDOMapper;
    @Autowired
    private ProductCustomerDOMapper productCustomerDOMapper;
    @Autowired
    private ProductDOMapper productDOMapper;
    @Autowired
    private StaffDOMapper staffDOMapper;
    @Autowired
    private SettingDOMapper settingDOMapper;
    @Autowired
    private FeedbackCustomerDOMapper feedbackCustomerDOMapper;
    @Autowired
    private FeedbackDOMapper feedbackDOMapper;


    @Override
    @Transactional
    public void addCustomer(CustomerDO customerDO,Integer staffId) throws BusinessException {
        //只支持经理增加用户
        //先插入customer表，再插入staff_customer表
        try{
            customerDOMapper.insertSelective(customerDO);
        }catch (DuplicateKeyException e){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"电话号码已被使用");
        }
        StaffCustomerDO staffCustomerDO = new StaffCustomerDO();
        staffCustomerDO.setCustomerId(customerDO.getId());
        staffCustomerDO.setStaffId(staffId);
        staffCustomerDOMapper.insertSelective(staffCustomerDO);
    }

    @Override
    public void updateCustomer(CustomerDO customerDO, Integer staffId) throws BusinessException {
        if (customerDO.getDetail() != null || customerDO.getName() != null){
            //查看该customer是否属于当前登录的staff或者是admin则也可更新
            StaffDO staffDO = staffDOMapper.selectByPrimaryKey(staffId);
            if (staffDO.getPrivilege() == 6){
                customerDOMapper.updateByPrimaryKeySelective(customerDO);
                return;
            }
            StaffCustomerDO staffCustomerDO = staffCustomerDOMapper.selectByCustomerId(customerDO.getId());
            if (staffCustomerDO != null && Objects.equals(staffId,staffCustomerDO.getStaffId())){
                //该客户属于该staff
                customerDOMapper.updateByPrimaryKeySelective(customerDO);
            }else {
                throw new BusinessException(EmBusinessError.NO_PERMISSION);
            }
        }
    }

    @Override
    @Transactional
    public void buyProductForCustomer(Integer customerId, Integer productId, Integer staffId) throws BusinessException {
        //三个表，product更新销量，product_customer关联，customer表更新积分，判断等级

        StaffDO staffDO = staffDOMapper.selectByPrimaryKey(staffId);
        ProductDO productDO = productDOMapper.selectByPrimaryKey(productId);
        //该产品积分
        Integer record = productDO.getRecord();
        CustomerDO customerDO = customerDOMapper.selectByPrimaryKey(customerId);
        Integer totalRecord = customerDO.getTotalRecord();
        int newRecord = totalRecord + record;
        String member = MemberUtil.getMember(newRecord);
        CustomerDO newCustomerDO = new CustomerDO();
        newCustomerDO.setId(customerId);
        newCustomerDO.setLevel(member);
        newCustomerDO.setTotalRecord(newRecord);
        if (staffDO.getPrivilege() == 6){
            //admin也可更新
            productDOMapper.updateSales(productId);
            ProductCustomerDO productCustomerDO = new ProductCustomerDO();
            productCustomerDO.setCustomerId(customerId);
            productCustomerDO.setProductId(productId);
            try{
                productCustomerDOMapper.insertSelective(productCustomerDO);
            }catch (DuplicateKeyException e){
                throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"已办理过该产品");
            }

            customerDOMapper.updateByPrimaryKeySelective(newCustomerDO);
        }

        StaffCustomerDO staffCustomerDO = staffCustomerDOMapper.selectByCustomerId(customerId);
        if (staffCustomerDO != null && Objects.equals(staffId,staffCustomerDO.getStaffId())){
            //该客户属于该staff
            productDOMapper.updateSales(productId);
            ProductCustomerDO productCustomerDO = new ProductCustomerDO();
            productCustomerDO.setCustomerId(customerId);
            productCustomerDO.setProductId(productId);
            try{
                productCustomerDOMapper.insertSelective(productCustomerDO);
            }catch (DuplicateKeyException e){
                throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"已办理过该产品");
            }

            customerDOMapper.updateByPrimaryKeySelective(newCustomerDO);
        }else {
            throw new BusinessException(EmBusinessError.NO_PERMISSION);
        }

    }

    @Override
    public CustomerDO getCustomerByCustomerId(Integer staffId,Integer customerId) throws BusinessException {
        //查看该customer是否属于当前登录的staff或者是admin则也可返回
        StaffDO staffDO = staffDOMapper.selectByPrimaryKey(staffId);
        if (staffDO.getPrivilege() == 6){
            CustomerDO customerDO = customerDOMapper.selectByPrimaryKey(customerId);
            return customerDO;
        }
        StaffCustomerDO staffCustomerDO = staffCustomerDOMapper.selectByCustomerId(customerId);
        if (staffCustomerDO != null && Objects.equals(staffId,staffCustomerDO.getStaffId())){
            //该客户属于该staff
            CustomerDO customerDO = customerDOMapper.selectByPrimaryKey(customerId);
            return customerDO;
        }else {
            throw new BusinessException(EmBusinessError.NO_PERMISSION);
        }
    }

    @Override
    public List<CustomerDO> getCustomersByStaffId(Integer staffId,Integer page,Integer rows) {
        PageHelper.startPage(page,rows);
        List<StaffCustomerDO> staffCustomerDOList = staffCustomerDOMapper.selectCustomersByStaffId(staffId);
        List<CustomerDO> customerDOList = staffCustomerDOList.stream().map(staffCustomerDO -> {
            CustomerDO customerDO = customerDOMapper.selectByPrimaryKey(staffCustomerDO.getCustomerId());
            return customerDO;
        }).collect(Collectors.toList());
        return customerDOList;
    }

    @Override
    public List<ProductModel> getProductsByCustomerId(Integer customerId,Integer page,Integer rows) {
        PageHelper.startPage(page,rows);
        List<ProductCustomerDO> productCustomerDOList = productCustomerDOMapper.selectProductsByCustomerId(customerId);
        List<ProductModel> productModelList = productCustomerDOList.stream().map(productCustomerDO -> {
            ProductDO productDO = productDOMapper.selectByPrimaryKey(productCustomerDO.getProductId());
            SettingDO settingDO = settingDOMapper.selectByPrimaryKey(productDO.getSortId());
            ProductModel productModel = this.convertFromDataObject(productDO, settingDO,productCustomerDO);
            return productModel;
        }).collect(Collectors.toList());
        return productModelList;
    }

    @Override
    public ProductModel getProductModelByCustomerId(ProductCustomerDO productCustomerDO) {
        ProductCustomerDO productCustomerDO1 = productCustomerDOMapper.selectByPrimaryKey(productCustomerDO);
        ProductDO productDO = productDOMapper.selectByPrimaryKey(productCustomerDO.getProductId());
        SettingDO settingDO = settingDOMapper.selectByPrimaryKey(productDO.getSortId());
        ProductModel productModel = this.convertFromDataObject(productDO, settingDO,productCustomerDO1);
        return productModel;
    }

    @Override
    public List<CustomerProductSort> getCustomerProductSort(Integer customerId) {
        List<CustomerProductSort> customerProductSortsList = productCustomerDOMapper.selectSortTotal(customerId);
        customerProductSortsList = customerProductSortsList.stream().map(customerProductSort -> {
            customerProductSort.setSortName(settingDOMapper.selectByPrimaryKey(customerProductSort.getSortId()).getValue());
            customerProductSort.setCustomerId(customerId);
            return customerProductSort;
        }).collect(Collectors.toList());
        return customerProductSortsList;
    }

    //根据客户id得到他的反馈
    @Override
    public List<FeedbackModel> getFeedbacksByCustomerId(Integer customerId) {
        List<FeedbackCustomerDO> feedbackCustomerDOList = feedbackCustomerDOMapper.selectFeedBackCustomerDOByCustomerId(customerId);
        List<FeedbackModel> feedbackModelList = feedbackCustomerDOList.stream().map(feedbackCustomerDO -> {
            FeedbackDO feedbackDO = feedbackDOMapper.selectByPrimaryKey(feedbackCustomerDO.getFeedbackId());
            SettingDO settingDO = settingDOMapper.selectByPrimaryKey(feedbackDO.getSortId());
            FeedbackModel feedbackModel = this.convertFromFeedbackDO(feedbackDO, settingDO);
            return feedbackModel;
        }).collect(Collectors.toList());
        return feedbackModelList;
    }

    private ProductModel convertFromDataObject(ProductDO productDO,SettingDO settingDO,ProductCustomerDO productCustomerDO){
        ProductModel productModel = new ProductModel();
        BeanUtils.copyProperties(productDO,productModel);
        productModel.setSort(settingDO.getValue());
        productModel.setBuyTime(productCustomerDO.getCreateTime());
        return productModel;
    }
    private FeedbackModel convertFromFeedbackDO(FeedbackDO feedbackDO, SettingDO settingDO){
        FeedbackModel feedbackModel = new FeedbackModel();
        BeanUtils.copyProperties(feedbackDO,feedbackModel);
        feedbackModel.setSortName(settingDO.getValue());
        return feedbackModel;
    }

}
