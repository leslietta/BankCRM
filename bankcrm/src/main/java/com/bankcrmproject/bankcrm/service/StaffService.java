package com.bankcrmproject.bankcrm.service;

import com.bankcrmproject.bankcrm.error.BusinessException;
import com.bankcrmproject.bankcrm.service.model.StaffModel;

import java.util.List;

public interface StaffService {

    StaffModel getStaffById(Integer id);

    StaffModel getStaffByEmail(String email);

    List<StaffModel> getAllStaffs(Integer staffId,Integer privilege,Integer page,Integer rows) throws BusinessException;

    void addStaff(StaffModel staffModel) throws BusinessException;

    void updateStaff(StaffModel staffModel) throws BusinessException;

    void deleteStaff(Integer id);

}
