package com.bankcrmproject.bankcrm.utils;

import com.bankcrmproject.bankcrm.config.Constants;

/**
 * @Description:
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/26 12:40
 */
public class MemberUtil {

    public static String getMember(Integer record){
        if (record>=0 && record<500){
            return Constants.ORDINARY_MEMBER;
        }else if (record>=500 && record<1500){
            return Constants.SILVER_MEMBER;
        }else if (record>=1500 && record<3000){
            return Constants.GOLD_MEMBER;
        }else if (record>=3000 && record<5000){
            return Constants.PLATINUM_MEMBER;
        }else if (record>=5000){
            return Constants.DIAMOND_MEMBER;
        }else {
            return Constants.ORDINARY_MEMBER;
        }
    }
}