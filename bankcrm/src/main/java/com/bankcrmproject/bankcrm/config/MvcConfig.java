package com.bankcrmproject.bankcrm.config;

import com.bankcrmproject.bankcrm.authorization.interceptor.AuthorizationInterceptor;
import com.bankcrmproject.bankcrm.authorization.resolvers.CurrentUserIdMethodArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @Description: 配置类，增加自定义拦截器和解析器
 * @see com.bankcrmproject.bankcrm.authorization.resolvers.CurrentUserIdMethodArgumentResolver
 * @see com.bankcrmproject.bankcrm.authorization.interceptor.AuthorizationInterceptor
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/13 22:23
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Autowired
    private CurrentUserIdMethodArgumentResolver currentUserIdMethodArgumentResolver;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor())
                .addPathPatterns("/**");    // 拦截所有请求，通过判断是否有 @Authorization 注解 决定是否需要登录
    }
    @Bean
    public AuthorizationInterceptor authenticationInterceptor() {
        return new AuthorizationInterceptor();
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(this.currentUserIdMethodArgumentResolver);
    }
    /*@Override
    public void addCorsMappings(CorsRegistry registry) {
        //设置允许跨域的路径
        registry.addMapping("/**")
                //设置允许跨域请求的域名
                .allowedOrigins("*")
                //是否允许证书 不再默认开启
                .allowCredentials(true)
                //设置允许的方法
                .allowedMethods("*")
                //跨域允许时间
                .maxAge(3600);
    }*/
}
