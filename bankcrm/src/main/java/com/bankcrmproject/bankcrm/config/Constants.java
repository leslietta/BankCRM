package com.bankcrmproject.bankcrm.config;

/**
 * @Description: 常量
 * @Author: leslie_tta@163.com
 * @Date: Created in 2019/4/10 20:12
 */
public class Constants {
    /**
     * 存储当前登录用户id的字段名
     */
    public static final String CURRENT_USER_ID = "CURRENT_USER_ID";

    /**
     * token有效期（小时）
     */
    public static final int TOKEN_EXPIRES_HOUR = 72;

    /**
     * 存放Authorization的header字段
     */
    public static final String AUTHORIZATION = "authorization";

    public static final String ORDINARY_MEMBER = "普通会员";
    public static final String SILVER_MEMBER = "白银会员";
    public static final String GOLD_MEMBER = "黄金会员";
    public static final String PLATINUM_MEMBER = "铂金会员";
    public static final String DIAMOND_MEMBER = "钻石会员";

}
