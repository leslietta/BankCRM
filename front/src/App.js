import React, { Component } from 'react';
import {LocaleProvider} from 'antd'
import {BrowserRouter} from 'react-router-dom'
import {Provider} from 'react-redux'
import {store} from './store'
import locale from 'antd/lib/locale-provider/zh_CN';
import moment from 'moment';
import Routes from "./route/Routes";


// 推荐在入口文件全局设置 locale
import 'moment/locale/zh-cn';
import Button from "./Button";
moment.locale('zh-cn');


class App extends Component {

  render() {
    return (
        <Provider store={store}>
                <LocaleProvider locale={locale}>
                    <BrowserRouter>
                        <Routes/>
                    </BrowserRouter>
                </LocaleProvider>
        </Provider>
    );
  }
}

export default App;
