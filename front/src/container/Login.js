import React,{Component} from 'react'
import {Form,Input,Button,Checkbox} from 'antd'
import '../assets/css/Login.css'

import {connect} from 'react-redux'
import {ACTION_AUTH,ACTION_NOTIFY} from '../store/action'
import logo from '../assets/images/slogo.png';

//util
import TokenManager from '../utils/TokenManager'
import HostPost from '../utils/HostPost'
import Notification from '../utils/Notification'
import DefaultHandlePostError from '../utils/DefaultHandlePostError'
import Logger from '../utils/Logger'
// import WtValidate from "../utils/WtValidate"
//const
const FormItem = Form.Item;

class LoginForm extends Component{
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                Logger.log('Received values of form: ', values);
                const {onFinish} = this.props;
                if(onFinish){
                    onFinish(values);
                }
            }
        });
    };

    render(){
        const {getFieldDecorator} = this.props.form;

        return(
            <Form
                style={{textAlign: 'left'}}
                onSubmit={this.handleSubmit}>
                <FormItem>
                    {getFieldDecorator('email', {
                        rules: [
                            { required: true, message:'请输入您的邮箱'},
                            // { validator: WtValidate.validateEmail }
                        ],
                    })(
                        <Input placeholder="邮箱" />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: '请输入密码!' }],
                    })(
                        <Input  type="password" placeholder="密码" />
                    )}
                </FormItem>
                <FormItem>
                    {
                        getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true
                        })(
                            <Checkbox style={{color: '#fff'}}>记住密码</Checkbox>
                        )
                    }
                    <a className="login-form-forgot">
                        忘记密码？
                    </a>
                    <Button
                        className="btn-login"
                        type="primary"
                        htmlType="submit"
                        loading={this.props.submitting}
                    >
                        登录
                    </Button>
                </FormItem>
            </Form>
        );
    }
}

const LoginFormWrapper = Form.create()(LoginForm);

class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            submitting:false,
        };
    }

    onFinish = (values) => {

        this.setState({submitting:true});
        HostPost('/auth/login',{
            email:values.email,
            password:values.password,
        }).then(({json,header}) => {
            if(json.status === "success"){
                TokenManager.setToken(json.data["token"],values.remember);
                //this.props.notifyChange(json.notify_count);
                this.props.login(json.info);
            } else if(json.status === "fail"){
                Notification.error('操作太过频繁，请稍后再试');
                this.setState({submitting:false});
            } else {
                throw json;
            }
        }).catch((error) => {
            Notification.error('用户名或密码错误');
            this.setState({submitting:false});
            DefaultHandlePostError(error);
        });
    };


    render(){
        return (
            <div className="page page-login vertical-align">
                <div className="page-content vertical-align-middle">
                    <div className="brand">
                        <img src={logo} alt="..."/>
                        <h2 className="brand-text">
                            银行CRM系统
                        </h2>
                    </div>
                    <p>请使用您的账号密码登录系统</p>
                    <LoginFormWrapper
                        onFinish={this.onFinish}
                        submitting={this.state.submitting}
                    />
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = {
    login:(userInfo) => {
        return {
            type:ACTION_AUTH.LOGIN,
            payload:{
                userInfo,
            }
        }
    },
    /*notifyChange:(count) => {
        return {
            type:ACTION_NOTIFY.CHANGE,
            payload:{
                count,
            }
        }
    }*/
};

export default connect(null,mapDispatchToProps)(Login);
//export default Login