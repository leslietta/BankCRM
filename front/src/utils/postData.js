import { isEmpty } from 'lodash';

/**
 * use to set data into FormData
 * @param {*} datas
 * @return {*} formData
 */
export const postData = (datas) => {
    if (isEmpty(datas)) {
        return null
    }
    let string = '';
    for(let name in datas){
        string += name+"="+datas[name]+"&";
    }

    return string.substr(0,string.length-1);
}

