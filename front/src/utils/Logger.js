const log = function ( ...values ){
    if(process.env.NODE_ENV === 'development'){
        console.log(values);
    }
};

export default {
    log,
};