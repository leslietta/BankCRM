import React,{Component} from 'react'
import { Layout, Menu, Dropdown, Icon} from 'antd';
import {Router,Link,Switch} from 'react-router-dom'
import HostPost from './utils/HostPost'
import {ACTION_AUTH,ACTION_NOTIFY} from './store/action'
import {connect} from 'react-redux'
import config from './config'
// import {wsHost} from "./config";
import TokenManager from './utils/TokenManager'
// import io from 'socket.io-client';
import './assets/css/Home.css'

const { Header, Content, Sider,Footer} = Layout;
const {SubMenu} = Menu;


class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    onLogout = () => {
        const token = TokenManager.getToken();
        if(!token){
            //如果没有token，则已经是退出了
            this.props.logout();
            return;
        }
        HostPost('/auth/logout',{token}).then(({json,header}) => {
            if(json.code === 0){
                TokenManager.removeToken();
                this.props.logout();
            }
        });
    };

    /*initPusher = () =>{
        const {userInfo} = this.props;
        this.socket = io(wsHost + '?userId='+userInfo.id);
        this.socket.on('message',(msgStr) => {
            console.log(msgStr);
            const msg = JSON.parse(msgStr);
            this.props.notifyChange(msg.count);
        });
    };

    uninitPusher = () => {
        if(this.socket){
            this.socket.disconnect();
            this.socket = null;
        }
    };

    componentDidMount(){
        this.initPusher();
    }

    componentWillUnmount(){
        this.uninitPusher();
    }*/


    renderSubMenu(){
        /*const {secondLevel} = this.props.match.params;
        const {notify_count} = this.props;*/

        return (
            <Menu
                theme="dark"
                mode="inline"
                defaultSelectedKeys={['patient-manage']}
                // selectedKeys={[secondLevel]}
            >
                <Menu.Item key="patient-manage">
                    <Link to="/home/user/patient-manage">
                        <Icon type="user" />
                        <span>用户管理</span>
                    </Link>
                </Menu.Item>
                <SubMenu key="s1" title={<span><Icon type="tags" /><span>商品管理</span></span>}>
                    <Menu.Item key="3">
                        <Link to="/category/first">
                            <span>一级分类</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="4">
                        <Link to="/category/second">
                            <span>二级分类</span>
                        </Link>
                    </Menu.Item>
                </SubMenu>
                <Menu.Item key="change-password">
                    <Link to="/home/user/change-password">
                        <Icon type="setting" />
                        <span>修改密码</span>
                    </Link>
                </Menu.Item>
            </Menu>
        );
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    renderOverlay = () => {
        return (
            <Menu>
                <Menu.Item>
                    {/* <Link to="/signin"> */}
                    退出
                    {/* </Link> */}
                </Menu.Item>
            </Menu>
        )
    };

    render(){
        const menu = this.renderOverlay();
        const collapsed=this.state.collapsed;
        // const {userInfo} = this.props;
        return (
            <div className="page page-home">
                <Layout>
                    <Sider
                        trigger={null}
                        collapsible
                        collapsed={this.state.collapsed}
                    >
                        {/*<div className="logo" style={{float:'center',height:62,marginTop:'8px',marginLeft:"25px"}}>
                            <img src={require('./assets/images/'+ci)} alt="" style={{}}/>
                        </div>*/}
                        {collapsed===true?
                            <div className="sidebar-logo">
                                <img src={require('./assets/images/'+"slogo.png")} alt=""/>
                            </div>
                            :
                            <div className="sidebar-Mlogo">
                                <img src={require('./assets/images/'+"logo.png")} alt=""/>
                            </div>
                        }
                        {this.renderSubMenu()}
                    </Sider>

                    <Layout>
                        {/*<Header style={{ background: '#fff', padding: 0 }}>
                            <Icon
                                className="trigger"
                                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                onClick={this.toggle}
                                style={{float:'left',color:'#808080',height:62}}
                            />
                            <div style={{float:'right',color:'#808080',height:62}}>
                            <WtNotificationPanel
                                style={{display:'inline-block'}}
                            />
                            <span color={'#808080'} >{userInfo.name}</span>
                            <a href="javascript:" style={{padding:'0 20px',color:'#808080'}} onClick={this.onLogout}>退出</a>
                        </div>
                        </Header>*/}
                        <Header>
                            <nav className="navbar">
                                <ul className="nav">
                                    <li className="nav-item">
                                        <Icon
                                            className="sidebar-trigger"
                                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                            onClick={this.toggle}
                                        />
                                    </li>
                                </ul>
                                <ul className="nav navbar-right">
                                    <Dropdown
                                        overlay={menu}
                                    >
                                        <li className="nav-item">
                                            <Icon
                                                type="user"
                                            />
                                        </li>
                                    </Dropdown>
                                </ul>
                            </nav>
                        </Header>

                        <Content style={{ padding: '20px 20px 0px 20px' }}>
                            <Layout className="mainLayout" style={{ padding: '24px 0', background: '#fff' }}>
                                <Content style={{ padding: '0 24px', minHeight: 780 }}>
                                    <Switch>
                                        //文件管理
                                        {/*<Route path="/home/user/file-simple-detail/:file_id" component={WtSimpleDetail}/>
                                    <Route path="/home/user/file-vep-detail/:analysisId/:filterResultId/:page" component={WtVepDetail}/>
                                    <Route path="/home/user/file-upload" component={WtUploadFile}/>*/}
                                    </Switch>
                                </Content>
                            </Layout>
                        </Content>
                        <Footer style={{ textAlign: 'center' }}>
                            BCRM ©2018 Created by Leslie. version {config.version}
                        </Footer>
                    </Layout>
                </Layout>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        userInfo: state.auth.userInfo,
        // notify_count: state.notify.count,
    }
};

const mapDispatchToProps = {
    logout:() => {
        return {
            type:ACTION_AUTH.LOGOUT,
        }
    },
    /*notifyChange:(count) => {
        return {
            type:ACTION_NOTIFY.CHANGE,
            payload:{
                count,
            }
        }
    }*/
};

export default connect(mapStateToProps,mapDispatchToProps)(Home);