import React,{Component} from "react"
import 'whatwg-fetch'
import { postData } from './utils/postData';

class Button extends Component{

    constructor(props){
        super(props);
        this.state = {
            a:''
        }
    }

    /*testBoot = () => {
        fetch('/user/get?id=1')
            .then((response) =>{
                return response.json();
            }).then((json) => {
            this.setState({
                a:JSON.stringify(json)
            })
            console.log('parsed json', json);
        }).catch((ex) => {
            console.log('msg',ex)
        })
    };*/
    testBoot = () => {
        let data = {
            id:1,
            name:"xxx",
            password:"ddd"
        };
        fetch('/user/get',{
            method:'POST',
            headers:{
                'Content-Type':'application/x-www-form-urlencoded;charset=utf8',
            },
            body:postData(data)
            //body:JSON.stringify(data)

        })
            .then((response) =>{
                return response.json();
            }).then((json) => {
            this.setState({
                a:JSON.stringify(json)
            });
            console.log('parsed json', json);
        }).catch((ex) => {
            console.log('msg',ex)
        })
    };

    render(){
        return (
            <div>
                <button id="button" onClick = {this.testBoot}>fetch</button>
                <p>{this.state.a}</p>
            </div>
        )
    }
}
export default Button