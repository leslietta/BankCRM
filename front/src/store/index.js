import {applyMiddleware,createStore,combineReducers,compose} from 'redux'
import thunk from 'redux-thunk'
import authReducer from './authReducer'
import notifyReducer from './notifyReducer'
const middlewares = [thunk];

const rootReducer = combineReducers({
	auth:authReducer,
	notify:notifyReducer,
});

export const store = createStore(
	rootReducer,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
	compose(
		applyMiddleware(...middlewares),
	)
);
