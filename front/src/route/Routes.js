import React, {Component} from 'react'
import {Route, Redirect} from 'react-router'
import {Router} from 'react-router-dom'
import history from './history'
import Login from '../container/Login'
import Home from '../Home'
import {connect} from 'react-redux'
import {ACTION_AUTH, ACTION_NOTIFY} from '../store/action'

//util
import HostPost from '../utils/HostPost'
import DefaultHandlePostError from '../utils/DefaultHandlePostError'

class Loading extends Component {
    render() {
        return 'loading';
    }
}


class Routes extends Component {
    loadUserInfo = () => {
        HostPost('/auth/info', {stage:'front'}, true).then(({json, header}) => {
            if (json.status === "success") {
                this.props.login(json.data);
                // this.props.notifyChange(json.notify_count);
            } else {
                throw json;
            }
        }).catch(e => {
            this.props.loginFail();
            DefaultHandlePostError(e);
        });
    };

    authCheck = (props) => {
        const {auth} = this.props;
        if (auth.login) {
            return <Home {...props} />;
        } else if (auth.try === false) {
            this.loadUserInfo();
            return <Loading/>
        } else {
            return <Redirect to="/login"/>;
        }
    };

    loginCheck = (props) => {
        const {auth} = this.props;
        if (auth.login) {
            return <Redirect to="/home/user/task-list"/>;
        } else if (auth.try === false) {
            this.loadUserInfo();
            return <Loading/>
        } else {
            return <Login {...props}/>;
        }
    };


    render() {
        return (
            <Router history={history}>
                <div style={{height: '100%'}}>
                    <Route path="/" render={this.loginCheck} exact/>
                    <Route path="/login" render={this.loginCheck}/>
                    <Route path="/home/:firstLevel/:secondLevel" render={this.authCheck}/>
                </div>
            </Router>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
    };
};

const mapDispatchToProps = {
    login: (userInfo) => {
        return {
            type: ACTION_AUTH.LOGIN,
            payload: {userInfo},
        }
    },
    loginFail: () => {
        return {
            type: ACTION_AUTH.LOGIN_FAIL,
        }
    },
    /*notifyChange: (count) => {
        return {
            type: ACTION_NOTIFY.CHANGE,
            payload: {
                count,
            }
        }
    }*/
};

export default connect(mapStateToProps, mapDispatchToProps)(Routes);